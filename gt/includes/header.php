<div class="header-container">
				<!-- header start -->
				<!-- classes:  -->
				<!-- "fixed": enables fixed navigation mode (sticky menu) e.g. class="header fixed clearfix" -->
				<!-- "dark": dark version of header e.g. class="header dark clearfix" -->
				<!-- "full-width": enables full-width menu -->
				<!-- ================ --> 
				<header class="header fixed  dark clearfix">
					
					<div class="container">
						<div class="row">

							<div class="col-md-3">

								<!-- header-left start -->
								<!-- ================ -->
								<div class="header-left clearfix">

									<!-- logo -->
									<div id="logo" class="logo">
                                                                            <a href="index.html"><img id="logo_img" src="images/logo_orange.png" alt="The Project" style="height: 100px;z-index: 1000;"></a>
									</div>
								</div>
								<!-- header-left end -->

							</div>
							<div class="col-md-9">
					
								<!-- header-right start -->
								<!-- ================ -->
								<div class="header-right clearfix">
									
								<!-- main-navigation start -->
								<!-- classes: -->
								<!-- "onclick": Makes the dropdowns open on click, this the default bootstrap behavior e.g. class="main-navigation onclick" -->
								<!-- "animated": Enables animations on dropdowns opening e.g. class="main-navigation animated" -->
								<!-- "with-dropdown-buttons": Mandatory class that adds extra space, to the main navigation, for the search and cart dropdowns -->
								<!-- ================ -->
								<div class="main-navigation  animated ">

									<!-- navbar start -->
									<!-- ================ -->
									<nav class="navbar navbar-default" role="navigation">
										<div class="container-fluid">

											<!-- Toggle get grouped for better mobile display -->
											<div class="navbar-header">
												<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
													<span class="sr-only">Toggle navigation</span>
													<span class="icon-bar"></span>
													<span class="icon-bar"></span>
													<span class="icon-bar"></span>
												</button>
												
											</div>

											<!-- Collect the nav links, forms, and other content for toggling -->
											<div class="collapse navbar-collapse" id="navbar-collapse-1">
												<!-- main-menu -->
												
												<!-- main-menu end -->
												<ul class="nav navbar-nav navbar-right">
													<li class="active">
														<a class="dropdown-toggle" data-toggle="dropdown" href="index.html">Home</a>
													</li>
													<li class="">
														<a href="#" class="dropdown-toggle" data-toggle="dropdown">Event</a>
<!--														<ul class="dropdown-menu">
															<li ><a href="#">Drive & Watch</a></li>
															<li ><a href="#">Dance With Rain</a></li>
															<li ><a href="#">Liven your taste Buds</a></li>
															<li ><a href="#">Play Pool</a></li>
															<li ><a href="#">Complete Entertainment</a></li>
															<li ><a href="#">When You Drink</a></li>
															<li ><a href="#">Lets your Family Play</a></li>
														</ul>-->
													</li>
													<!-- mega-menu end -->
													<li class="">
														<a class="dropdown-toggle" data-toggle="dropdown" href="#">Services</a>
													</li>
													<!-- mega-menu start -->													
													<li class="mega-menu narrow">
														<a href="#" class="dropdown-toggle" data-toggle="dropdown">About Us</a>
													</li>
													<!-- mega-menu end -->
													<li class="">
														<a href="portfolio-grid-2-3-col.html" class="dropdown-toggle" data-toggle="dropdown">Portfolio</a>
													</li>
													<li class="">
														<a href="index-shop.html" class="dropdown-toggle" data-toggle="dropdown">Gallery</a>
													</li>
													<li class="">
														<a href="blog-large-image-right-sidebar.html" class="dropdown-toggle" data-toggle="dropdown">Contact Us</a>
													</li>
												</ul>
											</div>

										</div>
									</nav>
									<!-- navbar end -->

								</div>
								<!-- main-navigation end -->	
								</div>
								<!-- header-right end -->
					
							</div>
						</div>
					</div>
					
				</header>
				<!-- header end -->
			</div>