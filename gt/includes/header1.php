<div class="header-container">
    
                                <div class="header-top dark " id="ht">
                                    <div class="container" >
						<div class="row">
							<div class="col-xs-3 col-sm-6 col-md-12 ">
								<!-- header-top-first start -->
								<!-- ================ -->
								<div class="header-top-first clearfix pull-left">
<!--									<ul class="social-links circle small clearfix hidden-xs">
										<li class="twitter"><a target="_blank" href="http://www.twitter.com/"><i class="fa fa-twitter"></i></a></li>
										<li class="skype"><a target="_blank" href="http://www.skype.com/"><i class="fa fa-skype"></i></a></li>
										<li class="linkedin"><a target="_blank" href="http://www.linkedin.com/"><i class="fa fa-linkedin"></i></a></li>
										<li class="googleplus"><a target="_blank" href="http://plus.google.com/"><i class="fa fa-google-plus"></i></a></li>
										<li class="youtube"><a target="_blank" href="http://www.youtube.com/"><i class="fa fa-youtube-play"></i></a></li>
										<li class="flickr"><a target="_blank" href="http://www.flickr.com/"><i class="fa fa-flickr"></i></a></li>
										<li class="facebook"><a target="_blank" href="http://www.facebook.com/"><i class="fa fa-facebook"></i></a></li>
										<li class="pinterest"><a target="_blank" href="http://www.pinterest.com/"><i class="fa fa-pinterest"></i></a></li>
									</ul>-->
<!--									<div class="social-links hidden-lg hidden-md hidden-sm circle small">
										<div class="btn-group dropdown">
											<button type="button" class="btn dropdown-toggle" data-toggle="dropdown"><i class="fa fa-share-alt"></i></button>
											<ul class="dropdown-menu dropdown-animation">
												<li class="twitter"><a target="_blank" href="http://www.twitter.com/"><i class="fa fa-twitter"></i></a></li>
												<li class="skype"><a target="_blank" href="http://www.skype.com/"><i class="fa fa-skype"></i></a></li>
												<li class="linkedin"><a target="_blank" href="http://www.linkedin.com/"><i class="fa fa-linkedin"></i></a></li>
												<li class="googleplus"><a target="_blank" href="http://plus.google.com/"><i class="fa fa-google-plus"></i></a></li>
												<li class="youtube"><a target="_blank" href="http://www.youtube.com/"><i class="fa fa-youtube-play"></i></a></li>
												<li class="flickr"><a target="_blank" href="http://www.flickr.com/"><i class="fa fa-flickr"></i></a></li>
												<li class="facebook"><a target="_blank" href="http://www.facebook.com/"><i class="fa fa-facebook"></i></a></li>
												<li class="pinterest"><a target="_blank" href="http://www.pinterest.com/"><i class="fa fa-pinterest"></i></a></li>
											</ul>
										</div>
									</div>-->
<ul class="list-inline hidden-sm hidden-xs h3-Brush-Script-MT">
										<!--<li><i class="fa fa-map-marker pr-5 pl-10"></i>Khata No 65/68 and 129/131 gurgaon - faridabad Rd.Gwalpahari,gurugram</li>-->
										<li><i class="fa fa-phone pr-5 pl-10"></i>+98993 00572</li>
										<li><i class="fa fa-envelope-o pr-5 pl-10"></i>gurgaontalkies@gmail.com</li>
									</ul>
								</div>
								<!-- header-top-first end -->
							</div>
						</div>
					</div>
				</div>
				<header class="header  fixed   clearfix header-gurgaon">
					
					<div class="container">
						<div class="row">
							<div class="col-md-3">
								<!-- header-left start -->
								<!-- ================ -->
								<div class="header-left clearfix">

									<!-- logo -->
									<div id="logo" class="logo">
                                                                            <a href="index.php"><img id="logo_img" class="swingimage" src="images/logo_light_blue.png" alt="The Gurgaon Talkies" style="
                                                                                                     "></a>
									</div>

								</div>
								<!-- header-left end -->
							</div>
							<div class="col-md-9">
					
								<!-- header-right start -->
								<!-- ================ -->
								<div class="header-right clearfix">
									
								<!-- main-navigation start -->
								<!-- classes: -->
								<!-- "onclick": Makes the dropdowns open on click, this the default bootstrap behavior e.g. class="main-navigation onclick" -->
								<!-- "animated": Enables animations on dropdowns opening e.g. class="main-navigation animated" -->
								<!-- "with-dropdown-buttons": Mandatory class that adds extra space, to the main navigation, for the search and cart dropdowns -->
								<!-- ================ -->
								<div class="main-navigation  animated with-dropdown-buttons">

									<!-- navbar start -->
									<!-- ================ -->
									<nav class="navbar navbar-default" role="navigation">
										<div class="container-fluid">

											<!-- Toggle get grouped for better mobile display -->
											<div class="navbar-header">
												<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
													<span class="sr-only">Toggle navigation</span>
													<span class="icon-bar"></span>
													<span class="icon-bar"></span>
													<span class="icon-bar"></span>
												</button>
												
											</div>

											<!-- Collect the nav links, forms, and other content for toggling -->
											<div class="collapse navbar-collapse " id="navbar-collapse-1">
												<!-- main-menu -->
												<ul class="nav navbar-nav ">
													<li class="active">
														<a class="dropdown-toggle" data-toggle="dropdown" href="index.html">Home</a>
													</li>
													<li class="">
														<a href="#" class="dropdown-toggle" data-toggle="dropdown">Event</a>
<!--														<ul class="dropdown-menu">
															<li ><a href="#">Drive & Watch</a></li>
															<li ><a href="#">Dance With Rain</a></li>
															<li ><a href="#">Liven your taste Buds</a></li>
															<li ><a href="#">Play Pool</a></li>
															<li ><a href="#">Complete Entertainment</a></li>
															<li ><a href="#">When You Drink</a></li>
															<li ><a href="#">Lets your Family Play</a></li>
														</ul>-->
													</li>
													<!-- mega-menu end -->
													<li class="">
														<a class="dropdown-toggle" data-toggle="dropdown" href="#">Services</a>
													</li>
													<!-- mega-menu start -->													
													<li class="mega-menu narrow">
														<a href="#" class="dropdown-toggle" data-toggle="dropdown">About Us</a>
													</li>
													<!-- mega-menu end -->
													<li class="">
														<a href="portfolio-grid-2-3-col.html" class="dropdown-toggle" data-toggle="dropdown">Portfolio</a>
													</li>
													<li class="">
														<a href="index-shop.html" class="dropdown-toggle" data-toggle="dropdown">Gallery</a>
													</li>
													<li class="">
														<a href="blog-large-image-right-sidebar.html" class="dropdown-toggle" data-toggle="dropdown">Contact Us</a>
													</li>
												</ul>
												<!-- main-menu end -->
												
												<!-- header dropdown buttons -->
												
												<!-- header dropdown buttons end-->
												
											</div>

										</div>
									</nav>
									<!-- navbar end -->

								</div>
								<!-- main-navigation end -->	
								</div>
								<!-- header-right end -->
					
							</div>
						</div>
					</div>
					
				</header>
				<!-- header end -->
			</div>