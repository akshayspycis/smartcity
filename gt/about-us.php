<html lang="en">
        <head>
		<meta charset="utf-8">
		<title> About Us | Gurgaon Talkies </title>
		<meta name="description" content="All the magic at one place.Movie and Food lovers, we bring all together.Cinema with big screen, Awesome Aquatic, Amazing Foods, Pool n Rain Parties">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<?php include 'includes/css.php';?>
                
	</head>
        <style>
            .title{
                color: #333;
                    font-family: cursive,sans-serif;
            }
            .about-us-discription{
                color: black;
                /*text-align: center;*/
            }
        </style>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans front-page ">

		<!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		
			<?php include 'includes/header.php';?>
		
                        <div class="banner dark-translucent-bg" style="background-image:url('images/about-us-1.jpg'); background-position: 50% 27%;">
				<!-- breadcrumb start -->
				<!-- ================ -->
				
				<div class="container">
					<div class="row">
						<div class="col-md-8 text-center col-md-offset-2 pv-20">
							<h3 class="title logo-font object-non-visible animated object-visible fadeIn" data-animation-effect="fadeIn" data-effect-delay="100">The <span class="text-default">Gurgaon</span> Talkies.</h3>
							<div class="separator object-non-visible mt-10 animated object-visible fadeIn" data-animation-effect="fadeIn" data-effect-delay="100"></div>
							<p class="text-center object-non-visible animated object-visible fadeIn" data-animation-effect="fadeIn" data-effect-delay="100">All the magic at one place.Movie and Food lovers, we bring all together.Cinema with big screen, Awesome Aquatic, Amazing Foods, Pool n Rain Parties.</p>
						</div>
					</div>
				</div>
			</div>
                    <section id="dishes" class="section default-bg clearfix" style=" background-color: #42275b;  "></section>
                        <section class="main-container padding-bottom-clear">

				<div class="container">
					<div class="row">

						<!-- main start -->
						<!-- ================ -->
						<div class="main col-md-12">
							<h3 class="title">Who <strong>We Are</strong></h3>
							<div class="separator-2"></div>
                                                        <br>
							<div class="row light-gray-bg">
                                                            <div class="col-md-7 " style="color: black;" >
									<p>Gurgaon Talkies shouts out to all the people who wish to party in style with foot-tapping music and flowing platters of mouth-watering delicacies to make the most out of their days.</p>
									<p>With summer in full swing, everyone’s got pool parties on their mind! Pool parties are a great way to get people together, whether you’re planning a party for children, adults, friends or family.</p>
									<ul class="list-icons">
										<li><i class="icon-check-1"></i> Fine Dining</li>
										<li><i class="icon-check-1"></i> Rain Dancing</li>
										<li><i class="icon-check-1"></i> Exquisite Bar</li>
										<li><i class="icon-check-1"></i> Fun Gaming</li>
										<li><i class="icon-check-1"></i> Relaxing Pool</li>
										<li><i class="icon-check-1"></i> Drive in Theater</li>
									</ul>
								</div>
								<div class="col-md-5">
									<div class="owl-carousel content-slider-with-controls">
										<div class="overlay-container overlay-visible">
                                                                                    <img src="images/ev-1.jpg" alt="">
											<div class="overlay-bottom hidden-xs">
												<div class="text">
													<h3 class="title">Fine Dining</h3>
												</div>
											</div>
											<a href="images/ev-1.jpg" class="popup-img overlay-link" title="image title"><i class="icon-plus-1"></i></a>
										</div>
										<div class="overlay-container overlay-visible">
											<img src="images/ev-2.jpg" alt="">
											<div class="overlay-bottom hidden-xs">
												<div class="text">
													<h3 class="title">Rain Dancing</h3>
												</div>
											</div>
											<a href="images/ev-2.jpg" class="popup-img overlay-link" title="image title"><i class="icon-plus-1"></i></a>
										</div>
										<div class="overlay-container overlay-visible">
											<img src="images/ev-3.jpg" alt="">
											<div class="overlay-bottom hidden-xs">
												<div class="text">
													<h3 class="title">Exquisite Bar</h3>
												</div>
											</div>
											<a href="images/ev-3.jpg" class="popup-img overlay-link" title="image title"><i class="icon-plus-1"></i></a>
										</div>
										<div class="overlay-container overlay-visible">
											<img src="images/ev-4.jpg" alt="">
											<div class="overlay-bottom hidden-xs">
												<div class="text">
                                                                                                    <h3 class="title">Fun Gaming</h3>
													
												</div>
											</div>
											<a href="images/ev-5.jpg" class="popup-img overlay-link" title="image title"><i class="icon-plus-1"></i></a>
										</div>
										<div class="overlay-container overlay-visible">
											<img src="images/ev-5.jpg" alt="">
											<div class="overlay-bottom hidden-xs">
												<div class="text">
                                                                                                    <h3 class="title">Relaxing Pool</h3>
													
												</div>
											</div>
											<a href="images/ev-5.jpg" class="popup-img overlay-link" title="image title"><i class="icon-plus-1"></i></a>
										</div>
										<div class="overlay-container overlay-visible">
											<img src="images/ev-6.jpg" alt="">
											<div class="overlay-bottom hidden-xs">
												<div class="text">
                                                                                                    <h3 class="title">Drive in Theater</h3>
													
												</div>
											</div>
											<a href="images/ev-6.jpg" class="popup-img overlay-link" title="image title"><i class="icon-plus-1"></i></a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- main end -->

					</div>
				</div>

                        </section>
                    <section class="main-container padding-bottom-clear">
				<div class="container">
					<div class="row">
						<div class="main col-md-12">
							<h3 class="title pull-right">Why <strong>Choose Us</strong></h3>
                                                        <p>&nbsp;</p>
							<div class="separator-3"></div>
                                                        <br>
							<div class="row light-gray-bg">
                                                            <div class="col-md-5">
									<div class="owl-carousel content-slider-with-controls">
										<div class="overlay-container overlay-visible">
                                                                                    <img src="images/about-us-2.jpg" alt="">
										</div>
									</div>
								</div>
                                                            <div class="col-md-7 about-us-discription " >
                                                                <p>&nbsp;</p>
									<p class="about-us-discription">Laser Vibgyor is India's premium Laser Multimedia Show Company which is offering a world of laser show entertainment. Laser shows special effects for any type of event from a product launch to birthday event, exhibitions to film festivals, Indoor or Outdoor we have the complete solution. High powered 30W RBG (Multi Color) Laser System to 20W GREEN Laser systems, which will make sure your event will look spectacular.</p>
                                                            </div>
							</div>
						</div>
					</div>
				</div>
                        </section>
                    <section class="main-container padding-bottom-clear">

				<div class="container">
					<div class="row">
						<div class="main col-md-12">
							<h3 class="title"><strong>Vision</strong></h3>
							<div class="separator-2"></div>
                                                        <br>
							<div class="row light-gray-bg">
                                                            <div class="col-md-7 about-us-discription " >
                                                                <p>&nbsp;</p>
									<p>To provide employment opportunities and economic development for North India, through tourism and destination family resort recreation.
                                                                            <br>
                                                                            We are committed to consistently providing enjoyable recreation, a safe environment, and memorable vacation experiences for every guest and every member of the staff.
                                                                        </p>
								</div>
								<div class="col-md-5">
									<div class="owl-carousel content-slider-with-controls">
										<div class="overlay-container overlay-visible">
                                                                                    <img src="images/vision.jpg" alt="">

										</div>
									</div>
								</div>
							</div>
                                                        <br>
                                                        <div class="separator-3"></div>
						</div>
						<!-- main end -->

					</div>
				</div>

                        </section>
                    <section class="main-container padding-bottom-clear">
				<div class="container">
					<div class="row">
						<div class="main col-md-12">
							<h3 class="title pull-right"><strong>Mission</strong></h3>
                                                        <p>&nbsp;</p>
							<div class="separator-3"></div>
                                                        <br>
							<div class="row light-gray-bg">
                                                            <div class="col-md-5">
									<div class="owl-carousel content-slider-with-controls">
										<div class="overlay-container overlay-visible">
                                                                                    <img src="images/mission.jpg" alt="">
										</div>
									</div>
								</div>
                                                            <div class="col-md-7 about-us-discription " >
                                                                <p>&nbsp;</p>
									<p class="about-us-discription">e will accomplish this through excellence in service, innovation, and anticipation of our guests ever changing needs and expectations. The ability to fulfill this commitment will be reflected in our profitability, continued growth, and the success of our company and each individual.</p>
                                                            </div>
							</div>
						</div>
					</div>
				</div>
                        </section>
                    <section class="main-container padding-bottom-clear">

				<div class="container">
					<div class="row">
						<div class="main col-md-12">
							<h3 class="title"><strong>Core Values</strong></h3>
							<div class="separator-2"></div>
                                                        <br>
							<div class="row light-gray-bg">
                                                            <div class="col-md-7 about-us-discription " >
                                                                <p>&nbsp;</p>
									<p>Among the standards, morals, ethics, ideals, and tenets of beliefs that we strive to encourage, uphold and sustain with our managerial and service staff, is for us to be relevantly creative but eco-minded, to build lasting rapport and relationships, to always be honest and professional, to extend compassion where required, and to be responsible for our own actions.
In addition, we strive to be approachable, are always truthful and sincere, to have dedicated passion for our work and duties, to co-exist as a team and to respect others irrespective of age, race and/or religion, to have courage to embrace change, and to deliver exceptional and exemplary service 24/7 to everyone and anyone who walks through our doors.
                                                                        </p>
								</div>
								<div class="col-md-5">
									<div class="owl-carousel content-slider-with-controls">
										<div class="overlay-container overlay-visible">
                                                                                    <img src="images/core-value.jpg" alt="">

										</div>
									</div>
								</div>
							</div>
                                                        <br>
                                                        <div class="separator-3"></div>
						</div>
						<!-- main end -->

					</div>
				</div>

                        </section>
                    <section class="main-container padding-bottom-clear">

				<div class="container">
					<div class="row">
						<div class="main col-md-12">
                                                    <h3 class="title"><strong>Testimonials on </strong><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJcAAAAaCAMAAACNbz5NAAAAyVBMVEX///8AAABJjC6MjIzU4s+9vb2goKClw5tLjTGfv5U5OTnBwcFGiyr5+/jX19fS0tL29vbf39//2IH/3oSSt4bq8ehnZ2cxMTF+fn7C1rzb5teMs3+oqKjo6OhsoFpWkz8pKSmtyKV4p2himk2ErXZxcXE+hx6zs7MPDw9YWFi4z7H63+AvgQBNTE3Qd3m1AADptbYgICBDQ0OzmmLVtnLuy30ACRJhVDaRe0qeiVl0Yjw2OT8nKzJQRjE8NirIrnX/6ozhoqMiHhU4B9JLAAAD9klEQVRIie2XbYOaRhDHdwBhkV3FE1CQJ0VA6ZlWc+klTdsk/f4fqjO7msg9aC590by4eXHszi7M72b+OyBjLzT/2gZ3nfTm8qUR+pa1893HPz9uTe9i5AjGVx6UC+t82vHuP1DZQ4A/3n94++H9PUDUC93WZxMfwL7GZfS41mL1Q0Sj0WjSAtzffZ5q+/zpb9iOya3WPSjPtw/m1wr5gCsMwh/iMuFo9++mNzc307u/To6BWh/D7GUPfMD1HZYkT2gQKwP1KMvS3+FuejN9C2WbZd4WYEirk9SEfZtO2CgdMTsaMRqPcdxGUarul7G7CGI97BaBRVyy66Re62TSqWNgLZ1YJS7sgsVSjZIulEvMZmJZyWMudjgpeQvvpndg6omtB7ZOXUpzlKDHSkyjCfaBvDMqad40jVAaCgtDNLfuCvNVNEu6O24q5jYuAla4q8nJJQTtd3DoNstK3CYsjOPwccLGGCyrYYdw+/vpP1vG0tnGpvpS1MwzofQGmQKc2xM2VFwAbYaijHDHIpbMKgw8dDnPExnzglssNip6doWIjoFcCyMIpYWsieBuKDsuMMMOLwqnw3wlyROVnw+xlBGGH7MWPsEEo6btJkKnrpOn9WVTstCOXJTiAUB2fEhsrJklCipPzJELiTCwJTBDimvFdaVxsKDLkhfExXX9wqfKiLkxd+yLbxLfbI9/Miv2kS/a9rmU3o5c+igcUcMkiXmOsVRDkJQv7Fo5dbL4yOXyQncxoVEkITm051mDlkX2b7++yYD5G9ixETB+K3cDNj/0uaIzrrkal9TLLCWwImcBd5VXn0cMrNKludjaMCp0hzqnjBG8XnmWK2VmxN78kpZ0NGcsA79zfCxUvbvOlaKOuWupfPW5lsZiRbU8RY8rLpYsNHpczgWu7RZRTGytHh4BQLHX5Xh0wKJtzOe5dCqpjrkKngiq45qcuo54QWPfuKiWQspeHS/ma4BSmhygHFCjAOoOKOta+zVX+ZhLCd7DiywEHXGSSnKue6XquMeFWxNW8UBlT+n+Ihcblr7+StC9ytPeCdSnAYx9/yHXcOKPN+TKuUMHryCZ81Uo40Lni0mjkicuucQG1XFD4sHlHfUVuusKVwYbeuNgD0sjSGswibE9Hj9GKqJ39YlrpriiDf0Le59ap1EVjVtU1Fe5MJpg0ehuFOgTSH1VFk1RCEEOV4iiEoJK7jQXuZgfAWxUpvyDytpmD0dlK+56uPOYF+nvCBM/Nkj35m6r9ZcEqyBhAYWQ7modsy44diXdw2N6SVnueu1oxVuLPF/EX1cuWpaatvcVszXNNru03Tyj/pnsletlNof6+qb/wfzs6o+PV3u1n9r+BZgqTdETdiSlAAAAAElFTkSuQmCC"/></h3>
							<div class="separator-2"></div>
                                                        <br>
							<div class="container">
					<div class="row">
						<div class="col-md-8 col-md-offset-2 about-us-discription light-gray-bg">
							<div class="testimonial text-center">
								<div class="testimonial-image">
                                                                    <img src="https://scontent.fblr6-1.fna.fbcdn.net/v/t31.0-8/23916722_1531878280212135_1840859371717502445_o.jpg?_nc_cat=0&oh=a9a229ac52bc710c9741f764eebee0ce&oe=5BA667A0" alt="Jane Doe" title="Jane Doe" class="img-circle" style="margin-left: 14px;">
								</div>
                                                            <h3 class="title">Just Perfect!</h3>
								<div class="separator"></div>
								<div class="testimonial-body">
									<blockquote>
										<p>It was fun, and lot relaxing, at the same time. Music, food and the company of friends makes for a wonderful retreat away from hustle-bustle of the city.</p>
									</blockquote>
									<div class="testimonial-info-1">Akshay Bilani</div>
									<div class="testimonial-info-2">By Infopark</div>
								</div>
							</div>
						</div>
					</div>
				</div>
                                                        <br>
                                                        <div class="separator-3"></div>
						</div>
						<!-- main end -->

					</div>
				</div>

                        </section>
                    
			<?php include 'includes/footer.php';?>
			<!-- footer end -->
			
		</div>
		
		<?php include 'includes/jslink.php';?>
		
		
	</body>
        <script>
            $(window).resize(function (){
//                document.location="index.php";
            })
            $(document).ready(function (){
               if($(window).width()<892){
                   setReposive();
               }
            });
            function setReposive(){
                
                $("#logo_img").css({'margin-top':'-5px'});
                $("#layer_trans_icon").css({'padding-left':'0px','padding-right':'0px'});
                $(".header-top").css({'display':'none'});
                $(".asas_asdas").css({'display':'none'});
            }
        </script>
</html>