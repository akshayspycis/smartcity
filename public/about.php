<!DOCTYPE html>
<html lang="en">
<head>
  <?php include 'includes/links.php';?>
    <style>
        #left-sidebar{
            margin-top:20px;
            min-height:700px;
            height: auto;
        }
        #right-content{
            margin-top:20px;
            height: auto;
             min-height:700px;
            border:1px solid #eaebeb;
        }
    </style>
</head>

<body>
    <!-- Fixed navbar -->
  <?php include 'includes/header.php';?>
      <header id="head" class="secondary">
        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <h1>About us</h1>
                </div>
            </div>
        </div>
    </header>
    <!-- container -->
    <section class="container">
        <div class="row">
            <div class="col-md-3" id="left-sidebar">
                    <ul class="nav nav-pills nav-stacked">
                    <li class="active"><a href="#">Home</a></li>
                    <li><a href="#">Menu 1</a></li>
                    <li><a href="#">Menu 2</a></li>
                    <li><a href="#">Menu 3</a></li>
                    </ul>
            </div>
            <div class="col-md-9" id="right-content">
                
            </div>

        </div>
    </section>
 
<?php include 'includes/footer.php';?>
</body>
</html>

