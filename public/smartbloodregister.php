<!DOCTYPE html>
<html lang="en">
<head>
  <?php include 'includes/links.php';?>
    <style>
        #left-sidebar{
            margin-top:20px;
            min-height:700px;
            height: auto;
        }
        #right-content{
            margin-top:20px;
            height: auto;
             min-height:700px;
            border-left:1px ridge #eaebeb;
        }
        #content-head{
           height:80px;
        }
        #content-head h2{
             margin:10px;
        } 
        #para-content{
            font-family:century gothic;
            line-height:23px;
            word-spacing:3px;
        }
    </style>
</head>

<body>
    <!-- Fixed navbar -->
  <?php include 'includes/header.php';?>
      <header id="head" class="secondary">
        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <h1>Smart Blood Bank</h1>
                </div>
            </div>
        </div>
    </header>
    <!-- container -->
    <section class="container">
        <div class="row">
            <div class="col-md-3" id="left-sidebar">
                    <ul class="nav nav-pills nav-stacked">
                    <li><a href="smartblood.php">Overview</a></li>
                    <li><a href="finddonar.php">Find a Donor</a></li>
                    <li class="active"><a href="smartbloodregister.php">Register Free</a></li>
                    <li><a href="needblood.php">Who Needs Blood</a></li>
                    <li><a href="donarsspeak.php">Donar's Speak</a></li>
                    </ul>
            </div>
            <div class="col-md-9" id="right-content">
                <div class="col-md-6" id="content-head">
                     <h3 class="section-title" style="font-family:century gothic;font-weight:bold;margin-top:30px;">Donor Registration</h3>
                </div>
                <div class="col-md-9" id ="para-content">
                
						
						
						<form class="form-light mt-20" action="insertdonor.php" method="post">
							<div class="form-group">
								<label>Name</label>
								<input type="text" name ="donarname" class="form-control" placeholder="Your name">
							</div>
                                                
                                                     <div class="row">
                                                                <div class="col-md-6">
									<div class="form-group">
                                                                            
										<label>Date of Birth</label>
                                                                                <div class="hero-unit">
                                                                                     <input type= "date" name ="dob" class= "form-control"/>
                                                                                </div>
										
                                                                                
									</div>
								</div>
                                                                <div class="col-md-6">
									<div class="form-group">
                                                                            
										<label>Last Donate</label>
                                                                                <div class="hero-unit">
                                                                                     <input type= "date" name ="lastdonate" class= "form-control"/>
                                                                                </div>
										
                                                                                
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Select Blood Group</label>
                                                                                 <select name="bloodgroup" class="form-control">
                                                                                    <option value="">Select</option>
                                                                                    <option value="A1+" >A1+ </option>
                                                                                    <option value="A1-" > A1-</option>
                                                                                    <option value="A2+" > A2+</option>
                                                                                    <option value="A2-" > A2- </option>
                                                                                    <option value="B+" > B+</option>
                                                                                    <option value="B-" > B-</option>
                                                                                    <option value="A1B+" > A1B+</option>
                                                                                    <option value="A1B-" > A1B-</option>
                                                                                    <option value="A2B+" > A2B+</option>
                                                                                    <option value="A2B-" > A2B-</option>
                                                                                    <option value="AB+" > AB+</option>
                                                                                    <option value="AB-" > AB-</option>	
                                                                                    <option value="O+" > O+	</option>
                                                                                    <option value="O-" > O-</option>
                                                                                    <option value="A+" > A+</option>
                                                                                    <option value="A-" > A-</option>				</select>
                                                                      </div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Gender</label>
                                                                                 <select name="gender" class="form-control">
                                                                                     <option value="Male">Male</option>
                                                                                    <option value="Female">Female</option>
                                                                                   
                                                                                  </select>
                                                                               
                                                                                
									</div>
								</div>
							</div>
                                                    <div class="row">
								
								<div class="col-md-6">
									<div class="form-group">
										<label>Weight</label>
                                                                               <input type="text" class="form-control" name="weight" placeholder="Your Weight in Kgs">
									
                                                                        </div>
								</div>
                                                       <div class="col-md-6">
									<div class="form-group">
										<label>Phone</label>
										<input type="text" name ="phone" class="form-control" placeholder="Phone number">
									</div>
								</div>
                                                       
							</div>
							<div class="row">
								
								 <div class="col-md-6">
									<div class="form-group">
										<label>Email</label>
										<input type="email" name ="email" class="form-control" placeholder="Email address">
									</div>
								</div>
                                                        <div class="col-md-6">            <div class="form-group">
								<label>City</label>
								<input type="text" disabled="disabled" name ="city" class="form-control" placeholder="Your City" Value="Bhopal">
							</div></div>     
                                                
							</div>
							
							<div class="form-group">
                                                                <label>Select Area</label>
                                                                                 <select name="area" class="form-control">
                                                                                    <option value="">Select</option>
                                                                                    <option value="Arera Colony" >Arera Colony </option>
                                                                                    <option value="Ashoka Garden" >Ashoka Garden</option>
                                                                                    <option value="Ayodhya Extention" >Ayodhya Extention</option>
                                                                                    <option value="Bairagargh" >Bairagargh</option>
                                                                                    <option value="Bhel" >Bhel</option>
                                                                                    <option value="Govindpura" >Govindpura</option>
                                                                                    <option value="Gandhi Nagar" >Gandhi Nagar</option>
                                                                                    <option value="Jahangirabad" >Jahangirabad</option>
                                                                                    <option value="Karond" >Karond</option>
                                                                                    <option value="Kolar" >Kolar</option>
                                                                                    <option value="Lal Ghati" >Lal Ghati</option>
                                                                                    <option value="MP Nagar" >MP Nagar</option>
                                                                                    <option value="New Market" >New Market</option>	
                                                                               </select>
                                                        </div>
							<div class="form-group">
								<label>Address</label>
								<textarea name ="address" class="form-control" id="message" placeholder="write your complete Address" style="height:100px;"></textarea>
							</div>
							<button type="submit" name="submit" class="btn btn-two">Save</button><p><br/></p>
						</form>
                
                </div>
            </div>

        </div>
    </section>
 
<?php include 'includes/footer.php';?>
    <script src="assets/js/jquery-1.9.1.min.js"></script>
        <script src="assets/js/bootstrap-datepicker.js"></script>
        <script type="text/javascript">
            // When the document is ready
            $(document).ready(function () {
                
                $('#pick').datepicker({
                    format: "dd/mm/yyyy"
                });  
            
            });
        </script>
</body>
</html>

