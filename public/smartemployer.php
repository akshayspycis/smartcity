<!DOCTYPE html>
<html lang="en">
<head>  <?php include 'includes/links.php';?>
    <style>
        #left-sidebar{
            margin-top:20px;
            min-height:700px;
            height: auto;
        }
        #right-content{
            margin-top:20px;
            height: auto;
             min-height:700px;
            border-left:1px ridge #eaebeb;
        }
        #content-head{
           height:80px;
        }
        #content-head h2{
             margin:10px;
        } 
        #para-content{
            font-family:century gothic;
            line-height:23px;
            word-spacing:3px;
        }
    </style>
</head>

<body>
    <!-- Fixed navbar -->
  <?php include 'includes/header.php';?>
      <header id="head" class="secondary">
        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <h1>Smart Employer</h1>
                </div>
            </div>
        </div>
    </header>
    <!-- container -->
    <section class="container">
        <div class="row">
            <div class="col-md-3" id="left-sidebar">
                   <ul class="nav nav-pills nav-stacked">
                        <li class="active"><a href="smartemployer.php">Found Jobs</a></li>
                        <li><a href="jobseeker.php">Job Seeker</a></li>
                        <li><a href="registration.php">Recruiter</a></li>
                       
                  </ul>
            </div>
             
               <div class="col-md-9" id="right-content">
               <h3 class="section-title" style="font-family:century gothic;font-weight:bold;margin-top:30px;">Search Your Dream Job</h3>
                <div class="col-md-9" id ="para-content">
               <p>&nbsp;</p>
               <div class="col-md-6">
                        <h4>Search Job</h4>	
                        <form class="form-light mt-20">
                        <div class="form-group">
                        <label>Company Name</label>
                        <input type="text" class="form-control" placeholder="Company Name">
                        </div>
                        <div class="form-group">
                        <label>Profile</label>
                        <input type="text" class="form-control" placeholder="Profile">
                        </div>
                        <div class="form-group">
                        <label>Experience</label>
                        <input type="text" class="form-control" placeholder="Experience">
                        </div>
                        <div class="form-group">
                        <label>Location</label>
                        <input type="text" class="form-control" placeholder="Location">
                        </div>
                        <div class="form-group">
                        <label>Package Offered</label>
                        <input type="text" class="form-control" placeholder="Package offered">
                        </div>
                        <button type="submit" class="btn btn-two">Search</button><p><br/></p>
                        </form>
                    </div>
                    <div class="col-md-6"  style="border-left:1px solid gainsboro">
                          <h4>Latest Jobs</h4>	
                    <div class="col-md-6">
                        <h5>Java Developer</h5>
                    </div>
                    </div>
								
								
							</div>
                                                   
							
  <p>&nbsp;</p>
 
           
                
                </div>
           

        </div>
        </div>
        
    </section>
 
<?php include 'includes/footer.php';?>
</body>
</html>

