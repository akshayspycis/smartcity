<!DOCTYPE html>
<html lang="en">
<head>
    
  <?php include 'includes/links.php';?>
    
    <style>
        #left-sidebar{
            margin-top:20px;
            min-height:700px;
            height: auto;
        }
        #right-content{
            margin-top:20px;
            height: auto;
             min-height:700px;
            border-left:1px ridge #eaebeb;
        }
        #content-head{
           height:80px;
        }
        #content-head h2{
             margin:10px;
        } 
        #para-content{
            font-family:century gothic;
            line-height:23px;
            word-spacing:3px;
        }
    </style>
</head>

<body>
    <!-- Fixed navbar -->
  <?php include 'includes/header.php';?>
      <header id="head" class="secondary">
        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <h1>Smart Sharing</h1>
                </div>
            </div>
        </div>
    </header>
    <!-- container -->
    <section class="container">
        <div class="row">
            <div class="col-md-3" id="left-sidebar">
                   <ul class="nav nav-pills nav-stacked">
                    <li class="active"><a href="traveltogether.php">Travel Together</a></li>
                    <li><a href="flatsharing.php">Flat Sharing</a></li>
                    <li><a href="booksharing.php">Book Sharing</a></li>
               </ul>
            </div>
            <div class="col-md-9" id="right-content">
                <h3 class="section-title" style="font-family:century gothic;font-weight:bold;margin-top:30px;">Travel together</h3>
                <div class="col-md-9" id ="para-content">
                    
                    <p>Be at the planned meeting point on time! Remember to bring exact change so you can give your car owner the agreed contribution during your journey.</p>
                    <p>&nbsp;</p>
                  	
                   
						
                                                    <div class="row">
                                                               <div class="col-md-6">
                                                                     <h4>Find a Ride</h4>	
                                                                    <form class="form-light mt-20" action="selecttravel.php" method="post">
									<div class="form-group">
                                                                            <label>From</label>
										<input type="text" name = "fromcity" class="form-control" placeholder="Leaving from">
									</div>
									<div class="form-group">
                                                                            <label>To</label>
										<input type="text" name = "tocity" class="form-control" placeholder="Going to">
									</div>
									<div class="form-group">
                                                                            <label>Date</label>
										 <input type= "date" name = "date" class= "form-control"/>
									</div>
									<button type="submit" name = "select" class="btn btn-two">Search</button><p><br/></p>
						            </form>
								</div>
                                                               <div class="col-md-6"  style="border-left:1px solid gainsboro">
                                                                    <form class="form-light mt-20" action="inserttravel.php" method="post">
                                                                          <h4>After Ride</h4>	
									<div class="form-group">
                                                                            <label>Leaving From</label>
										<input type="text"  name = "fromcity" class="form-control" placeholder="Leaving From City">
									</div>
									<div class="form-group">
                                                                            <label>To</label>
										<input type="text"  name = "tocity" class="form-control" placeholder="Going to City">
									</div>
                                                                           
                                                                          <div class="form-group">
                                                                            <label>Date</label>
                                                                        <input type= "date"  name = "date" class= "form-control"/>
									</div>
                                                                              
                                                          
                                                                         
                                                                          <div class="form-group">
                                                                              <label>Time</label>
										<input type="time"  name = "time" class="form-control" />
                                                                            </div>
                                                            
                                                                          <div class="form-group">
                                                                              <label>Contact</label>
										<input type="text"  name = "contact" class="form-control" placeholder="Contact">
									</div>
                                                                          <div class="form-group">
                                                                              <label>No. of Seats Available</label>
										<input type="text"   name = "seats" class="form-control" placeholder="Seats">
									</div>
                                                                          <div class="form-group">
                                                                              <label>Vehicle No.</label>
										<input type="text"  name = "vehicle" class="form-control" placeholder="Vehicle No.">
									</div>
                                                                          <div class="form-group">
                                                                              <label>Fare</label>
										<input type="text"  name = "fare" class="form-control" placeholder="Fare">
									</div>
                                                                        <button type="submit" name = "save" class="btn btn-two">Save</button><p><br/></p>
						        </form>
								</div>
								
								
							</div>
                                                   
							
  <p>&nbsp;</p>
 
           
                
                </div>
           

        </div>
            </div>
    </section>
 
<?php include 'includes/footer.php';?>
</body>
</html>

