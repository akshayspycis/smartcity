<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'includes/links.php';?>
</head>
<body>
	<!-- Fixed navbar -->
	<?php include 'includes/header.php';?>
	<!-- /.navbar --> 

	<!-- Header -->
	<header id="head">
		<div class="container">
			<div class="banner-content">
				<div id="da-slider" class="da-slider">
					<div class="da-slide">
						<h2>Smart Blood Bank</h2>
						<p>More then 5000 blood donar</p>
						<div class="da-img"></div>
					</div>
					<div class="da-slide">
						<h2>Smart Education</h2>
						<p>More then 1000 Institutes</p>
						<div class="da-img"></div>
					</div>
					<div class="da-slide">
						<h2>Smart Sharing</h2>
						<p>Share your Space</p>
						<div class="da-img"></div>
					</div>
                                    <div class="da-slide">
						<h2>Smart Problem Solving</h2>
						<p>Solve your problem in a minutes</p>
						<div class="da-img"></div>
					</div>
                                    <div class="da-slide">
						<h2>Smart Social work</h2>
						<p>Do Great Work</p>
						<div class="da-img"></div>
                                    </div>
                                     <div class="da-slide">
						<h2>Smart Jobs</h2>
						<p>Get your Dream Job</p>
						<div class="da-img"></div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<!-- /Header -->


	<div id="courses">
		<div class="container">
			<h2>Welcome to Smart City</h2>
			<div class="row">
				<div class="col-md-4">
					<div class="featured-box">
						<i class="fa fa-cogs fa-2x"></i>
						<div class="text">
							<h3>Smart Blood Bank</h3>
                                                        <p>Welcome to Smart Blood Bank.With support of the Red Cross Society, Youth Red Cross to promote the voluntary blood donation movement</p>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="featured-box">
						<i class="fa fa-leaf fa-2x"></i>
						<div class="text">
							<h3>Smart Education</h3>
                                                        <p>Take online courses that are fun and engaging. Pass exams to earn real college credit. Research schools and degrees to further your education.</p>
                                                        
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="featured-box">
						<i class="fa fa-tachometer fa-2x"></i>
						<div class="text">
							<h3>Smart Sharing</h3>
							<p>Rent out your room, house or apartment on Airbnb. Join thousands already renting out their space to people all over the world. Listing your space is free!</p>
                                                       
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="featured-box">
						<i class="fa fa-eye fa-2x"></i>
						<div class="text">
							<h3>Problem Solving</h3>
							<p>Social problem-solving, in its most basic form, is defined as problem solving as it occurs in the natural environment.</p>
                                                       
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="featured-box">
						<i class="fa fa-quote-right fa-2x"></i>
						<div class="text">
							<h3>Smart Social Work</h3>
							<p>Make online donations for school childrens. Please donate to hunger child.Support plan's work towards betterment of children .</p>
                                                       
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="featured-box">
						<i class="fa fa-arrows fa-2x"></i>
						<div class="text">
							<h3>Smart Employment
                                                        </h3>
							<p>Find the Best Jobs.India's No.1 Job Portal. Search & Apply for Job Vacancies across Top Companies in India. Post your Resume to us</p>
                                                       
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
	<!-- container -->
<!--	<section class="container">
		<div class="heading">
			 Heading 
			<h2>Our Students</h2>
		</div>
		<div class="row">
			<div class="col-md-4">
				<img src="assets/images/1.jpg" alt="" class="img-responsive">
			</div>
			<div class="col-md-8">
				<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. </p>
				<blockquote class="blockquote-1">
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid</p>
					<small>Someone famous in <cite title="Source Title">Source Title</cite></small>
				</blockquote>
			</div>
		</div>
	</section>-->
<?php include 'includes/footer.php';?>
</body>
</html>
