<!DOCTYPE html>
<html lang="en">
<head>
  <?php include 'includes/links.php';?>
    <style>
        #left-sidebar{
            margin-top:20px;
            min-height:700px;
            height: auto;
        }
        #right-content{
            margin-top:20px;
            height: auto;
             min-height:700px;
            border-left:1px ridge #eaebeb;
        }
        #content-head{
           height:80px;
        }
        #content-head h2{
             margin:10px;
        } 
        #para-content{
            font-family:century gothic;
            line-height:23px;
            word-spacing:3px;
        }
    </style>
</head>

<body>
    <!-- Fixed navbar -->
  <?php include 'includes/header.php';?>
      <header id="head" class="secondary">
        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <h1>Smart Education</h1>
                </div>
            </div>
        </div>
    </header>
    <!-- container -->
    <section class="container">
        <div class="row">
            <div class="col-md-3" id="left-sidebar">
                   <ul class="nav nav-pills nav-stacked">
                       <li class="active"><a href="smarteducation.php">Education Point</a></li>
                    <li><a href="findteacher.php">Find Teacher</a></li>
                    <li><a href="findstudents.php">Find Student</a></li>
                   
                   
               </ul>
            </div>
            <div class="col-md-9" id="right-content">
                <h3 class="section-title" style="font-family:century gothic;font-weight:bold;margin-top:30px;">Education</h3>
                <div class="col-md-9" id ="para-content">
                    
                   
                    <p>&nbsp;</p>
                  	
                   
						
                                                    <div class="row">
                                                               <div class="col-md-6">
                                                                     <h4>Student</h4>	
                                                                    <form class="form-light mt-20" action="insertstudent.php" method="post">
									<div class="form-group">
                                                                            <label>Name</label>
										<input type="text" name= "name" class="form-control" placeholder="your name">
									</div>
									<div class="form-group">
                                                                            <label>Branch/Class</label>
										<input type="text" name= "branch" class="form-control" placeholder="branch/class">
									</div>
									<div class="form-group">
                                                                            <label>Want to Study</label>
										<input type="text" name= "subject" class="form-control" placeholder="subject">
									</div>
									<div class="form-group">
                                                                            <label>Contact No</label>
										<input type="text" name= "contact" class="form-control" placeholder="contact No.">
									</div>
                                                                   
                                                                        <button type="submit" name="save1" class="btn btn-two">Save</button><p><br/></p>
						            </form>
								</div>
                                                               <div class="col-md-6"  style="border-left:1px solid gainsboro">
                                                                    <form class="form-light mt-20" action="insertteacher.php" method="post">
                                                                          <h4>Teacher</h4>	
									<div class="form-group">
                                                                            <label>Name</label>
										<input type="text" name= "name" class="form-control" placeholder="Name">
									</div>
									<div class="form-group">
                                                                            <label>Qualifications</label>
										<input type="text" name="qualification" class="form-control" placeholder="Qualifications">
									</div>
                                                                         
                                                                          <div class="form-group">
                                                                            <label>Specialization</label>
										<input type="text" name="subject" class="form-control" placeholder="Specialization">
									</div>
                                                                             
                                                                 
                                                                          <div class="form-group">
                                                                              <label>Contact No.</label>
										<input type="text" name="contact" class="form-control" placeholder="TContact No.">
                                                                            </div>
                                                                         
                                                                         
                                                                        <button type="submit" name="save2" class="btn btn-two">Save</button><p><br/></p>
						        </form>
								</div>
								
								
							</div>
                                                   
							
  <p>&nbsp;</p>
 
           
                
                </div>
           

        </div>
            </div>
    </section>
 
<?php include 'includes/footer.php';?>
</body>
</html>

