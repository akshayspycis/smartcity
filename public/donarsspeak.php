<!DOCTYPE html>
<html lang="en">
<head>
  <?php include 'includes/links.php';?>
    <style>
        #left-sidebar{
            margin-top:20px;
            min-height:700px;
            height: auto;
        }
        #right-content{
            margin-top:20px;
            height: auto;
             min-height:700px;
            border-left:1px ridge #eaebeb;
        }
        #content-head{
           height:80px;
        }
        #content-head h2{
             margin:10px;
        } 
        #para-content{
            font-family:century gothic;
            line-height:23px;
            word-spacing:3px;
        }
    </style>
</head>

<body>
    <!-- Fixed navbar -->
  <?php include 'includes/header.php';?>
      <header id="head" class="secondary">
        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <h1>Smart Blood Bank</h1>
                </div>
            </div>
        </div>
    </header>
    <!-- container -->
    <section class="container">
        <div class="row">
            <div class="col-md-3" id="left-sidebar">
                   <ul class="nav nav-pills nav-stacked">
                    <li><a href="smartblood.php">Overview</a></li>
                    <li><a href="finddonar.php">Find a Donor</a></li>
                    <li><a href="smartbloodregister.php">Register Free</a></li>
                    <li><a href="needblood.php">Who Needs Blood</a></li>
                    <li class="active"><a href="donarsspeak.php">Donar's Speak</a></li>
                    </ul>
            </div>
            <div class="col-md-9" id="right-content">
                
                <div class="col-md-9" id ="para-content">
                    <div style="height:auto;padding-bottom:20px;border-bottom:1px dashed #006dcc;">
                         <p>I have donated blood more than seven times and have'nt had any problems due to donation.</p>
                         <p style="float:right">Ankit</p>
                    </div>
                    <div style="height:auto;padding-bottom:20px;border-bottom:1px dashed #006dcc;margin-top:20px;">
                         <p>I have donated blood on many occasions and believe me while donating I always feel pride from inside, a feeling of saving someone's life is beyond anything else. We all should experiance such feelings in life..........</p>
                         <p style="float:right">Ruchir</p>
                    </div>
                    <div style="height:auto;padding-bottom:20px;border-bottom:1px dashed #006dcc;margin-top:20px;">
                         <p>I brought awareness of blood donation among my friends and made two others regular donors and they felt very happy participating in social activities. I think more publicity is required in rural areas to bring awareness among the donors.</p>
                         <p style="float:right">Lalit</p>
                    </div>
                    <div style="height:auto;padding-bottom:20px;border-bottom:1px dashed #006dcc;margin-top:20px;">
                         <p>I am a regular donor who normally donates blood on the occasion of my birthday or my daughter's birthday, else on demand. I am so happy that I have done a good job through net by registering on BharatBloodBank.com</p>
                         <p style="float:right">Ram</p>
                    </div>
                    <div style="height:auto;padding-bottom:20px;border-bottom:1px dashed #006dcc;margin-top:20px;">
                         <p>I donated blood to a needy patient undergoing open heart surgery at GKNM Hospital, Coimbatore. I am happy to be associated with BharatBloodBank and thank them for giving me this opportunity.</p>
                         <p style="float:right">Deepak</p>
                    </div>
                   
                    
                
  <p>&nbsp;</p>
  <a href="smartbloodregister.php" style="height:50px;width:auto;padding:10px;background:green;color:white"> Start by Ragistering as Donar</a>
           
                
                </div>
            </div>

        </div>
    </section>
 
<?php include 'includes/footer.php';?>
</body>
</html>

