<!DOCTYPE html>
<html lang="en">
<head>
  <?php include 'includes/links.php';?>
    <style>
        #left-sidebar{
            margin-top:20px;
            min-height:700px;
            height: auto;
        }
        #right-content{
            margin-top:20px;
            height: auto;
             min-height:700px;
            border-left:1px ridge #eaebeb;
        }
        #content-head{
           height:80px;
        }
        #content-head h2{
             margin:10px;
        } 
        #para-content{
            font-family:century gothic;
            line-height:23px;
            word-spacing:3px;
        }
    </style>
</head>

<body>
    <!-- Fixed navbar -->
  <?php include 'includes/header.php';?>
      <header id="head" class="secondary">
        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <h1>Smart City</h1>
                </div>
            </div>
        </div>
    </header>
    <!-- container -->
    <section class="container">
        <div class="row">
            <div class="col-md-3" id="left-sidebar">
                    <ul class="nav nav-pills nav-stacked">
                     <li class="active"><a href="SignUp.php">Sign Up</a></li>
                     <li><a href="login.php">Login</a></li>
                   </ul>
            </div>
            <div class="col-md-9" id="right-content">
                <div class="col-md-6" id="content-head">
                     <h3 class="section-title" style="font-family:century gothic;font-weight:bold;margin-top:30px;">Sign UP</h3>
                </div>
                <div class="col-md-9" id ="para-content">
                
		<form class="form-light mt-20" action="" method="post">
                             
									<div class="form-group">
                                                                            <label>Name</label>
										<input type="text" name= "name" class="form-control" placeholder="your name">
									</div>
									<div class="form-group">
                                                                            <label>Username</label>
										<input type="text" name= "branch" class="form-control" placeholder="username">
									</div>
									<div class="form-group">
                                                                            <label>Password</label>
										<input type="text" name= "subject" class="form-control" placeholder="">
									</div>
									<div class="form-group">
                                                                            <label>Contact No</label>
										<input type="text" name= "contact" class="form-control" placeholder="contact No.">
									</div>
									<div class="form-group">
                                                                            <label>Gender</label>
										 <select name="gender" class="form-control">
                                                                                     <option value="0">Select Gender</option>
                                                                                     <option value="Male">Male</option>
                                                                                    <option value="Female">Female</option>
                                                                                 </select>
									</div>
									<div class="form-group">
                                                                            <label>Email</label>
										<input type="text" name= "contact" class="form-control" placeholder="contact No.">
									</div>
                                                                   
                                                                        <button type="submit" name="save1" class="btn btn-two">Save</button><p><br/></p>
						            </form>
                
                </div>
            </div>

        </div>
    </section>
 
<?php include 'includes/footer.php';?>
  
</body>
</html>

