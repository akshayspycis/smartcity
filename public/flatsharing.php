<!DOCTYPE html>
<html lang="en">
<head>
  <?php include 'includes/links.php';?>
    <style>
        #left-sidebar{
            margin-top:20px;
            min-height:700px;
            height: auto;
        }
        #right-content{
            margin-top:20px;
            height: auto;
             min-height:700px;
            border-left:1px ridge #eaebeb;
        }
        #content-head{
           height:80px;
        }
        #content-head h2{
             margin:10px;
        } 
        #para-content{
            font-family:century gothic;
            line-height:23px;
            word-spacing:3px;
        }
    </style>
</head>

<body>
    <!-- Fixed navbar -->
  <?php include 'includes/header.php';?>
      <header id="head" class="secondary">
        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <h1>Smart Sharing</h1>
                </div>
            </div>
        </div>
    </header>
    <!-- container -->
    <section class="container">
        <div class="row">
            <div class="col-md-3" id="left-sidebar">
                   <ul class="nav nav-pills nav-stacked">
                    <li><a href="traveltogether.php">Travel Together</a></li>
                    <li class="active"><a href="flatsharing.php">Flat Sharing</a></li>
                    <li><a href="booksharing.php">Book Sharing</a></li>
               </ul>
            </div>
            <div class="col-md-9" id="right-content">
                <h3 class="section-title" style="font-family:century gothic;font-weight:bold;margin-top:30px;">Flat Sharing</h3>
                <div class="col-md-9" id ="para-content">
                    
                    <p>Smart Flat Sharing provide a platform to share your space with needy..</p>
                    <p>&nbsp;</p>
                  	
                   
						
                                                    <div class="row">
                                                               <div class="col-md-6">
                                                                     <h4>Share a Flat</h4>	
                                                                    <form class="form-light mt-20" action="insertflat.php" method="post">
									<div class="form-group">
                                                                            <label>Number of Person</label>
										<input type="text" name ="persons" class="form-control" placeholder="Number of persons">
									</div>
                                                                        <div class="form-group">
                                                                            <label>City</label>
										<input type="text"  name = "city" class="form-control" placeholder="Your City" value = "Bhopal">
									</div>
                                                                        <div class="form-group">
                                                                <label>Select Area</label>
                                                                                 <select name="area" class="form-control">
                                                                                    <option value="">Select</option>
                                                                                    <option value="Arera Colony" >Arera Colony </option>
                                                                                    <option value="Ashoka Garden" >Ashoka Garden</option>
                                                                                    <option value="Ayodhya Extention" >Ayodhya Extention</option>
                                                                                    <option value="Bairagargh" >Bairagargh</option>
                                                                                    <option value="Bhel" >Bhel</option>
                                                                                    <option value="Govindpura" >Govindpura</option>
                                                                                    <option value="Gandhi Nagar" >Gandhi Nagar</option>
                                                                                    <option value="Jahangirabad" >Jahangirabad</option>
                                                                                    <option value="Karond" >Karond</option>
                                                                                    <option value="Kolar" >Kolar</option>
                                                                                    <option value="Lal Ghati" >Lal Ghati</option>
                                                                                    <option value="MP Nagar" >MP Nagar</option>
                                                                                    <option value="New Market" >New Market</option>	
                                                                                  </select>
                                                                            </div>
									<div class="form-group">
                                                                            <label>Type of flat</label>
										<label class="radio-inline">
                                                                                  <input type="radio" name="flatetype" value="rental">Rental
                                                                                </label>
                                                                                <label class="radio-inline">
                                                                                  <input type="radio" name="flatetype" value="owned">Owned
                                                                                </label>
                                                                               
									</div>
									<div class="form-group">
                                                                            <label>Age</label>
										<input type="text" name= "age" class="form-control" placeholder="Age">
									</div>
									
                                                                   
                                                                        <button type="submit" name= "inserflate" class="btn btn-two">Save</button><p><br/></p>
						            </form>
								</div>
                                                               <div class="col-md-6"  style="border-left:1px solid gainsboro">
                                                                    <form class="form-light mt-20" action="selectflat.php" method="post">
                                                                          <h4>Need A Flat</h4>	
									<div class="form-group">
                                                                            <label>City</label>
										<input type="text" name= "city" class="form-control" placeholder="City" value="Bhopal" disabled="disabled">
									</div>
                                                                         <div class="form-group">
                                                                        <label>Select Area</label>
                                                                                 <select name="area" class="form-control">
                                                                                    <option value="">Select</option>
                                                                                    <option value="Arera Colony" >Arera Colony </option>
                                                                                    <option value="Ashoka Garden" >Ashoka Garden</option>
                                                                                    <option value="Ayodhya Extention" >Ayodhya Extention</option>
                                                                                    <option value="Bairagargh" >Bairagargh</option>
                                                                                    <option value="Bhel" >Bhel</option>
                                                                                    <option value="Govindpura" >Govindpura</option>
                                                                                    <option value="Gandhi Nagar" >Gandhi Nagar</option>
                                                                                    <option value="Jahangirabad" >Jahangirabad</option>
                                                                                    <option value="Karond" >Karond</option>
                                                                                    <option value="Kolar" >Kolar</option>
                                                                                    <option value="Lal Ghati" >Lal Ghati</option>
                                                                                    <option value="MP Nagar" >MP Nagar</option>
                                                                                    <option value="New Market" >New Market</option>	
                                                                                   				
                                                                                 </select>
                                                        </div>
									<div class="form-group">
                                                                            <label>Type of flat</label>
										<label class="radio-inline">
                                                                                  <input type="radio" name="flatetype" value="rental">Rental
                                                                                </label>
                                                                                <label class="radio-inline">
                                                                                  <input type="radio" name="flatetype" value="owned">Owned
                                                                                </label>
                                                                               
									</div>
                                                                         
                                                                          
                                                                       
                                                                         
                                                                        <button type="submit" name="search" class="btn btn-two">Search</button><p><br/></p>
						        </form>
								</div>
								
								
							</div>
                                                   
							
  <p>&nbsp;</p>
 
           
                
                </div>
           

        </div>
    </section>
 
<?php include 'includes/footer.php';?>
</body>
</html>

