	<footer id="footer">
		<div class="container" id ="topfooter">
			<div class="social text-center">
				<a href="#"><i class="fa fa-twitter"></i></a>
				<a href="#"><i class="fa fa-facebook"></i></a>
				<a href="#"><i class="fa fa-dribbble"></i></a>
				<a href="#"><i class="fa fa-flickr"></i></a>
				<a href="#"><i class="fa fa-github"></i></a>
			</div>

			<div class="clear"></div>
			<!--CLEAR FLOATS-->
		</div>
		<div class="footer2">
			<div class="container">
				<div class="row">

					<div class="col-md-10 panel">
						<div class="panel-body">
							<p class="simplenav">
                                                            <a href="../index.php">Home</a> | 
								<a href="">Smart Blood Bank</a> |
								<a href="">Smart Education</a> |
								<a href="">Smart Sharing</a> |
								<a href="">Problem Solving</a> |
                                                                <a href="">Smart Employment</a> |
                                                                 <a href="">Smart Social Work</a> |
								<a href="">Contact</a>
							</p>
						</div>
					</div>

					<div class="col-md-6 panel">
						<div class="panel-body">
							<p class="text-right">
								
							</p>
						</div>
					</div>

				</div>
				<!-- /row of panels -->
			</div>
		</div>
	</footer>

	<!-- JavaScript libs are placed at the end of the document so the pages load faster -->
	<script src="assets/js/modernizr-latest.js"></script>
	<script src="assets/js/jquery.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/jquery.cslider.js"></script>
	<script src="assets/js/custom.js"></script>
        <script src="assets/js/bootstrap-datepicker.js"></script>
          <!-- Load jQuery and bootstrap datepicker scripts -->
  
      
       