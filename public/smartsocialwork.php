<!DOCTYPE html>
<html lang="en">
<head>
  <?php include 'includes/links.php';?>
    <style>
        #left-sidebar{
            margin-top:20px;
            min-height:700px;
            height: auto;
        }
        #right-content{
            margin-top:20px;
            height: auto;
             min-height:700px;
            border-left:1px ridge #eaebeb;
        }
        #content-head{
           height:80px;
        }
        #content-head h2{
             margin:10px;
        } 
        #para-content{
            font-family:century gothic;
            line-height:23px;
            word-spacing:3px;
        }
    </style>
</head>

<body>
    <!-- Fixed navbar -->
  <?php include 'includes/header.php';?>
      <header id="head" class="secondary">
        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <h1>Smart Social Work</h1>
                </div>
            </div>
        </div>
    </header>
    <!-- container -->
    <section class="container">
        <div class="row">
            <div class="col-md-3" id="left-sidebar">
                   <ul class="nav nav-pills nav-stacked">
                    <li class="active"><a href="#">Overview</a></li>
                    
                    </ul>
            </div>
            <div class="col-md-9" id="right-content">
                <div class="col-md-6" id="content-head">
                    <h2>Share something for needy</h2>
                </div>
                <div class="col-md-9" id ="para-content">
                 <form class="form-light mt-20" action="insertsocial.php" method="post">
			<div class="form-group">
                        <label>Name</label>
			<input type="text" name = "name" class="form-control" placeholder="Your Name">
		</div>
		<div class="form-group">
               <label>Select Area</label>
               <select name="area" class="form-control">
                <option value="">Select</option>
                <option value="Arera Colony" >Arera Colony </option>
                <option value="Ashoka Garden" >Ashoka Garden</option>
                <option value="Ayodhya Extention" >Ayodhya Extention</option>
                <option value="Bairagargh" >Bairagargh</option>
                <option value="Bhel" >Bhel</option>
                <option value="Govindpura" >Govindpura</option>
                <option value="Gandhi Nagar" >Gandhi Nagar</option>
                <option value="Jahangirabad" >Jahangirabad</option>
                <option value="Karond" >Karond</option>
                <option value="Kolar" >Kolar</option>
                <option value="Lal Ghati" >Lal Ghati</option>
                <option value="MP Nagar" >MP Nagar</option>
                <option value="New Market" >New Market</option>	

             </select>
		</div>
		<div class="form-group">
                  <label>Share Things</label>
			<select name="things" class="form-control">
                <option value="none">Select</option>
                <option value="Food" >Food</option>
                <option value="Clothes" >Clothes</option>
                <option value="Books" >Books</option>
               </select>
		</div>
              <div class="form-group">
                        <label>Contact</label>
			<input type="text" name = "contact" class="form-control" placeholder="Contact">
             </div>
		 <button type="submit" name = "insert" class="btn btn-two">Search</button><p><br/></p>
</form>
  <p>&nbsp;</p>
 </div>
            </div>

        </div>
    </section>
 
<?php include 'includes/footer.php';?>
</body>
</html>

