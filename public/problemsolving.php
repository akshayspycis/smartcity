<!DOCTYPE html>
<html lang="en">
<head>
  <?php include 'includes/links.php';?>
    <style>
        #left-sidebar{
            margin-top:20px;
            min-height:700px;
            height: auto;
        }
        #right-content{
            margin-top:20px;
            height: auto;
             min-height:700px;
            border-left:1px ridge #eaebeb;
        }
        #content-head{
           height:80px;
        }
        #content-head h2{
             margin:10px;
        } 
        #para-content{
            font-family:century gothic;
            line-height:23px;
            word-spacing:3px;
        }
    </style>
</head>

<body>
    <!-- Fixed navbar -->
  <?php include 'includes/header.php';?>
      <header id="head" class="secondary">
        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <h1>Smart Problems Solver</h1>
                </div>
            </div>
        </div>
    </header>
    <!-- container -->
    <section class="container">
        <div class="row">
            <div class="col-md-3" id="left-sidebar">
                    <ul class="nav nav-pills nav-stacked">
                    <li class="active"><a href="#">Overview</a></li>
                   </ul>
            </div>
            <div class="col-md-9" id="right-content">
                <div class="col-md-6" id="content-head">
                     <h3 class="section-title" style="font-family:century gothic;font-weight:bold;margin-top:30px;">Raise Your Problem</h3>
                </div>
                <div class="col-md-9" id ="para-content">
                
						
						
						<form class="form-light mt-20" action="insertproblems.php" method="post">
							<div class="form-group">
								<label>Name</label>
								<input type="text" name="name" class="form-control" placeholder="Your name">
							</div>
                                                   
                                                   
                                                     <div class="row">
                                                                <div class="col-md-6">
									<div class="form-group">
                                                                            
										<label>Date</label>
                                                                                <div class="hero-unit">
                                                                                    <input type ="date" name = "date" class="form-control">
                                                                                </div>
										
                                                                                
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Problem Related To</label>
                                                                                 <select name="problem" class="form-control">
                                                                                    <option value="">Select</option>
                                                                                    <option value="Garbage " >Garbage </option>
                                                                                    <option value="Drainage" >Drainage</option>
                                                                                    <option value="Street Light" >Street Light</option>
                                                                                    <option value="Fallen Tree" >Fallen Tree</option>
                                                                                    <option value="Died Animals" >Died Animals</option>
                                                                                    <option value="Electricity" >Electricity</option>
                                                                                    <option value="Water Supply" >Water Supply</option>
                                                                                    <option value="Water Supply" >Corruption</option>
                                                                                  
                                                                                  				</select>
                                                                              
                                                                               
                                                                                
									</div>
								</div>
								
							</div>
                                                    <div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>Gender</label>
                                                                                 <select name="gender" class="form-control">
                                                                                     <option value="Male">Male</option>
                                                                                    <option value="Female">Female</option>
                                                                                   
                                                                                  </select>
                                                                               
                                                                                
									</div>
								</div>
								
							</div>
                                                     <div class="form-group">
								<label>Message</label>
								<textarea class="form-control" name ="message" id="message" placeholder="write your Problem" style="height:100px;"></textarea>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>Email</label>
										<input type="email" name ="email" class="form-control" placeholder="Email address">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Phone</label>
										<input type="text" name ="contact" class="form-control" placeholder="Phone number">
									</div>
								</div>
							</div>
                                                   
							<div class="form-group">
								<label>City</label>
								<input type="text" name ="city" class="form-control" placeholder="Your City" value= "Bhopal">
							</div>
							<div class="form-group">
                                                                <label>Select Area</label>
                                                                                 <select name="area" class="form-control">
                                                                                    <option value="">Select</option>
                                                                                    <option value="Arera Colony" >Arera Colony </option>
                                                                                    <option value="Ashoka Garden" >Ashoka Garden</option>
                                                                                    <option value="Ayodhya Extention" >Ayodhya Extention</option>
                                                                                    <option value="Bairagargh" >Bairagargh</option>
                                                                                    <option value="Bhel" >Bhel</option>
                                                                                    <option value="Govindpura" >Govindpura</option>
                                                                                    <option value="Gandhi Nagar" >Gandhi Nagar</option>
                                                                                    <option value="Jahangirabad" >Jahangirabad</option>
                                                                                    <option value="Karond" >Karond</option>
                                                                                    <option value="Kolar" >Kolar</option>
                                                                                    <option value="Lal Ghati" >Lal Ghati</option>
                                                                                    <option value="MP Nagar" >MP Nagar</option>
                                                                                    <option value="New Market" >New Market</option>	
                                                                                   				
                                                                                 </select>
                                                        </div>
							<div class="form-group">
								<label>Address</label>
								<textarea class="form-control" name= "address" id="message" placeholder="write your complete Address" style="height:100px;"></textarea>
							</div>
							<button type="submit" name="save" class="btn btn-two">Save</button><p><br/></p>
						</form>
                
                </div>
            </div>

        </div>
    </section>
 
<?php include 'includes/footer.php';?>
    <script src="assets/js/jquery-1.9.1.min.js"></script>
        <script src="assets/js/bootstrap-datepicker.js"></script>
        <script type="text/javascript">
            // When the document is ready
            $(document).ready(function () {
                
                $('#pick').datepicker({
                    format: "dd/mm/yyyy"
                });  
            
            });
        </script>
</body>
</html>

