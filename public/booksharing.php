<!DOCTYPE html>
<html lang="en">
<head>
  <?php include 'includes/links.php';?>
    <style>
        #left-sidebar{
            margin-top:20px;
            min-height:700px;
            height: auto;
        }
        #right-content{
            margin-top:20px;
            height: auto;
             min-height:700px;
            border-left:1px ridge #eaebeb;
        }
        #content-head{
           height:80px;
        }
        #content-head h2{
             margin:10px;
        } 
        #para-content{
            font-family:century gothic;
            line-height:23px;
            word-spacing:3px;
        }
    </style>
</head>

<body>
    <!-- Fixed navbar -->
  <?php include 'includes/header.php';?>
      <header id="head" class="secondary">
        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <h1>Smart Sharing</h1>
                </div>
            </div>
        </div>
    </header>
    <!-- container -->
    <section class="container">
        <div class="row">
            <div class="col-md-3" id="left-sidebar">
                   <ul class="nav nav-pills nav-stacked">
                    <li><a href="traveltogether.php">Travel Together</a></li>
                    <li><a href="flatsharing.php">Flat Sharing</a></li>
                    <li class="active"><a href="booksharing.php">Book Sharing</a></li>
               </ul>
            </div>
            <div class="col-md-9" id="right-content">
                <h3 class="section-title" style="font-family:century gothic;font-weight:bold;margin-top:30px;">Book Sharing</h3>
                <div class="col-md-9" id ="para-content">
             
                    <p>&nbsp;</p>
                  	
                   
						
                                                    <div class="row">
                                                               <div class="col-md-6">
                                                                     <h4>Share Book</h4>	
                                                                    <form class="form-light mt-20" action="insertbook.php" method="post">
									<div class="form-group">
                                                                            <label>Book Name</label>
										<input type="text"  name = "bookname" class="form-control" placeholder="book name">
									</div>
									<div class="form-group">
                                                                            <label>Eddition</label>
										<input type="text"  name = "edition" class="form-control" placeholder="Eddition">
									</div>
									<div class="form-group">
                                                                            <label>Cost per Day</label>
										<input type="text" class="form-control" name = "cost"  placeholder="Cost">
									</div>
									<div class="form-group">
                                                                            <label>Contact</label>
										<input type="text" class="form-control" name = "contact"  placeholder="Contact">
									</div>
									<button type="submit" name = "save" class="btn btn-two">Search</button><p><br/></p>
						            </form>
								</div>
                                                               <div class="col-md-6"  style="border-left:1px solid gainsboro">
                                                                    <form class="form-light mt-20" action="selectbook.php" method="post">
                                                                          <h4>Need Book</h4>	
									<div class="form-group">
                                                                            <label>Book Name</label>
										<input type="text" class="form-control" name = "name" placeholder="Book Name">
									</div>
									
                                                                       
                                                                        <button type="submit" name = "selectbook" class="btn btn-two">Search</button><p><br/></p>
						        </form>
								</div>
								
								
							</div>
                                                   
							
  <p>&nbsp;</p>
 
           
                
                </div>
           

        </div>
    </section>
 
<?php include 'includes/footer.php';?>
</body>
</html>

