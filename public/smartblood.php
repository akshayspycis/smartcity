<!DOCTYPE html>
<html lang="en">
<head>
  <?php include 'includes/links.php';?>
    <style>
        #left-sidebar{
            margin-top:20px;
            min-height:700px;
            height: auto;
        }
        #right-content{
            margin-top:20px;
            height: auto;
             min-height:700px;
            border-left:1px ridge #eaebeb;
        }
        #content-head{
           height:80px;
        }
        #content-head h2{
             margin:10px;
        } 
        #para-content{
            font-family:century gothic;
            line-height:23px;
            word-spacing:3px;
        }
    </style>
</head>

<body>
    <!-- Fixed navbar -->
  <?php include 'includes/header.php';?>
      <header id="head" class="secondary">
        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <h1>Smart Blood Bank</h1>
                </div>
            </div>
        </div>
    </header>
    <!-- container -->
    <section class="container">
        <div class="row">
            <div class="col-md-3" id="left-sidebar">
                   <ul class="nav nav-pills nav-stacked">
                    <li class="active"><a href="smartblood.php">Overview</a></li>
                    <li><a href="finddonar.php">Find a Donor</a></li>
                    <li><a href="smartbloodregister.php">Register Free</a></li>
                    <li><a href="needblood.php">Who Needs Blood</a></li>
                    <li><a href="donarsspeak.php">Donar's Speak</a></li>
                    </ul>
            </div>
            <div class="col-md-9" id="right-content">
                <div class="col-md-6" id="content-head">
                    <h2>Why Donate Blood?</h2>
                </div>
                <div class="col-md-9" id ="para-content">
                    <p>Blood is the living fluid that all life is based on. Blood is composed of 60% liquid part and 40% solid part. The liquid part called Plasma, made up of 90% water and 10% nutrients, hormones, etc. is easily replenished by food, medicines, etc. But the solid part that contains RBC (red blood cells), WBC (white blood cells) and Platelets take valuable time to be replaced if lost.</p>
                 <h2>When is blood needed?</h2>
                 <p>Blood is needed at regular intervals and at all times as it has only finite time of storage. Red blood cells can be stored for about 42 days, fresh frozen plasma and cryoprecipitate for 365 days and blood platelets for 5 days.</p>
                 <p>As you know blood cannot be harvested it can only be donated. This means only you can save a life that needs blood.</p>
                 <p>Every year India requires 40 million units of 250cc blood out of which only a meager 500,000 of blood units are available.</p>
                 <p>Saving a life does not require heroic deeds. You could just do it with a small thought and an even smaller effort by saying "yes".</p>
                
  <p>&nbsp;</p>
  <a href="smartbloodregister.php" style="height:50px;width:auto;padding:10px;background:green;color:white"> Start by Ragistering as Donar</a>
           
                
                </div>
            </div>

        </div>
    </section>
 
<?php include 'includes/footer.php';?>
</body>
</html>

