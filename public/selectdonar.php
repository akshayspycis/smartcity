<?php
   include 'dbconnection.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?php include 'includes/links.php';?>
    <style>
        #left-sidebar{
            margin-top:20px;
            min-height:700px;
            height: auto;
        }
        #right-content{
            margin-top:20px;
            height: auto;
             min-height:700px;
            border-left:1px ridge #eaebeb;
        }
        #content-head{
           height:80px;
        }
        #content-head h2{
             margin:10px;
        } 
        #para-content{
            font-family:century gothic;
            line-height:23px;
            word-spacing:3px;
        }
        #select tr th{
            background:#0088cc;
            color:white;
            
        }
        #select tr th:nth-child(1){
            background:#0088cc;
            
        }
        #select tr td:nth-child(2){
           
            color:black;
            font-weight:bold;
            
        }
        #select tr td:nth-child(4){
       
            color:black;
            font-weight:bold;
            
        }
        #select tr td:nth-child(8){
          
            color:black;
            font-weight:bold;
            
        }
    </style>
</head>

<body>
    <!-- Fixed navbar -->
  <?php include 'includes/header.php';?>
      <header id="head" class="secondary">
        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <h1>Blood Bank</h1>
                </div>
            </div>
        </div>
    </header>
    <!-- container -->
    <section class="container">
        <div class="row">
            
            
                <div class="col-md-6" id="content-head">
                     <h3 class="section-title" style="font-family:century gothic;font-weight:bold;margin-top:30px;">Selected Results</h3>
                </div>
               
                <div class="col-md-12" id ="para-content" style="min-height:500px;">
                    <div class="row">
                       
                       
         <?php 
            if(isset($_POST['submit'])) {
         $bloodgroup = $_POST['bloodgroup'];
        $area = $_POST['area'];
        $db = mysql_select_db("smartcity", $conn)or die("could not select Database".' '.mysql_error());
       
        $sql = "select * from bloodbank where bloodgroup = '$bloodgroup' AND area = '$area'";
        
        $result = mysql_query($sql, $conn) or die('Could not run query'.mysql_error());
         
    
    ?>
                        <table class="table table-hover" id ="select" width="100%">
                                <thead>
                                    <tr>
                                        <td style="font-family:century gothic;font-weight:bold;">Total Search Results : <?php $rowcount = mysql_num_rows($result);echo $rowcount;?></td>
                                    </tr>
                                     
                                <tr>
                                <th width="10%">Donar_Id</th>
                                <th>Name</th>
                                 <th>Dob</th>
                                <th>B-Group</th>
                                <th>Gender</th>
                                <th>Weight</th>
                                <th>Email</th>
                               
                                <th>Mobile</th>
                                <th>City</th>
                                <th>Area</th>
                                <th>Last Donate</th>
                                </tr>
                                </thead>
                                <tbody>
                            <?php    while ($row = mysql_fetch_array($result)) { ?>
                                     <tr>
                                         <td><?php echo $row['id'];?></td>
                                         <td><?php echo $row['name'];?></td>
                                         <td><?php echo $row['dob'];?></td>
                                         <td><?php echo $row['bloodgroup'];?></td>
                                         <td><?php echo $row['gender'];?></td>
                                         <td><?php echo $row['weight'];?></td>
                                         <td><?php echo $row['email'];?></td>
                                         <td><?php echo $row['phone'];?></td>
                                         <td><?php echo $row['city'];?></td>
                                         <td><?php echo $row['area'];?></td>
                                         <td><?php echo $row['lastdonate'];?></td>
                                         
                                
                                </tr>
                                    <?php
                                          
                                    }
                                    }
 else { echo 'values are not set';}
                              ?>
                               
                              </tbody>
                                </table>               
             
                     
                  
                    </div>
                </div>
        

        </div>
    </section>
 
<?php include 'includes/footer.php';?>
    <script src="assets/js/jquery-1.9.1.min.js"></script>
        <script src="assets/js/bootstrap-datepicker.js"></script>
        <script type="text/javascript">
            // When the document is ready
            $(document).ready(function () {
                
                $('#pick').datepicker({
                    format: "dd/mm/yyyy"
                });  
            
            });
        </script>
</body>
</html>

